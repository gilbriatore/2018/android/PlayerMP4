package br.edu.up.playermp4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    String res = "android.resource://" +  getPackageName();
    String path = res + "/" + R.raw.big_buck_bunny;

    //http://tylergrund.com/mp3/Beatles/01%20Happiness%20Is%20A%20Warm%20Gun.mp3

    path = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    Log.d("x", "PATH-------> " + path);

    VideoView videoView = (VideoView) findViewById(R.id.videoView);
    videoView.setMediaController(new MediaController(this));
    videoView.setVideoPath(path);
    videoView.start();
  }
}
